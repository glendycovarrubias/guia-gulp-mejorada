
    const gulp              = require('gulp'),
        dateFormat          = require('dateformat');
        $                   = require('gulp-load-plugins')(),
        paths               = require('./gulp.config.json'),
        pkg                 = require('./package.json'),
        banner              = createComment(paths.config.banner)
        notify          = require('gulp-notify')
    ;

    pkg.datetime        = dateFormat(new Date(), 'dddd, mmmm dS, yyyy, h:MM:ss TT');
    pkg.date            = dateFormat(new Date(), 'yyyy');



    //Arrow function 
    gulp.task('sass', () => {
        // console.log('sass');
        
        // gulp-ruby-sass
        return $.rubySass(paths.source.scss, { precision : 8, style : 'expanded' })
            .on("error", $.notify.onError((error) => '¡Ouch! Pasó lo siguiente: ' + error.message))
            .pipe($.plumber())
            .pipe($.autoprefixer({ browsers: paths.config.autoprefixerBrowsers }))
            .pipe($.rename({ basename: paths.config.name }))
            .pipe($.header(banner, { pkg : pkg }))
            .pipe(gulp.dest(paths.dest.css))
            .pipe($.size({ showFiles : true }))
            .pipe($.rename({ suffix : paths.config.minSufix }))
            .pipe($.cssnano({ autoprefixer: false, safe: true }))
            .pipe(gulp.dest(paths.dest.css))
            .pipe($.size({ showFiles : true }))
            .pipe($.notify({message: 'Sass Complete'}));
    });

/**
 * [Tarea que se encarga de minificar los archivos js del proyecto]
 *
 * @author   Glendy Covarrubias <glendycovarrubias@hotmail.com>
 * date 2017-09-13
 * @version [1.0]
 * @param   {[type]} ){ gulp.src('web/public/src/js*.js')   .pipe(concat('main.min.js'))                .pipe(minifyJS())   .on("error", errorAlertJS)      .pipe(plumber())    .pipe(gulp.dest('web/public/js'))} [description]
 * @return  {[type]}                                                                    [description]
 */
    gulp.task('js', () => {
        return gulp.src(paths.source.js)
            .pipe($.babel({ presets: ['env'] }))
            .on("error", errorAlertJS)
            .pipe($.plumber())
            .pipe($.order(paths.dest.orderJs))
            .pipe($.concat("main.js"))
            .pipe($.rename({ basename: paths.config.name }))
            .pipe($.header(banner, { pkg : pkg }))
            .pipe(gulp.dest(paths.dest.js))
            .pipe($.size({ showFiles : true }))
            .pipe($.rename({ suffix : paths.config.minSufix }))
            .pipe($.uglify())
            .pipe($.header(banner, { pkg : pkg }))
            .pipe(gulp.dest(paths.dest.js))
            .pipe($.size({ showFiles : true }))
            .pipe($.notify({message: 'Js Complete'}))
        ;
    });

/**
 * [Funcion que evita que el gulp watch sea detenido por algun error de copilacion(sintaxis etc..)
 * y te señale el error y donde se encuentra  ALERT PARA EL JS]
 *
 * @author   Glendy Covarrubias <glendycovarrubias@hotmail.com>
 * date 2017-09-13
 * @version [1.0]
 * @param   {[type]} error [description]
 * @return  {[type]}       [description]
 */
function errorAlertJS(error){
    //Aqui configuramos el titulo y subtitulo del mensaje de error,
    $.notify.onError({
        title    : "Gulp JavaScript",
        subtitle : "Algo esta mal en tu javascript",
        sound    : "Basso"
    })(error);

    //También podemos pintar el error en el terminal
    console.log(error.toString());
    this.emit("end");
}


/**
 * [Funcion que evita que el gulp watch sea detenido por algun error de copilacion(sintaxis etc..)]
 * y te señala el error y donde se encuentra ALERT PARA EL CSS
 *
 * @author   Glendy Covarrubias <glendycovarrubias@hotmail.com>
 * date 2017-09-24
 * @version [1.0]
 * @param   {[type]} error [description]
 * @return  {[type]}       [description]
 */
function errorAlertSass(error){
    //Aqui configuramos el titulo y subtitulo del mensaje de error,
    $.notify.onError({
        title    : "Gulp Sass",
        subtitle : "Algo esta mal en tu Sass",
        sound    : "Basso"
    })(error);

    //También podemos pintar el error en el terminal
    console.log(error.toString());
    this.emit("end");
}


    /**
     * Facilita la generación del banner para los archivos generados
     * @param  array comments Arreglo con el texto deseado
     * @return string El banner ya formateado
     */
    function createComment(comments)
    {
        var output = `/*!\n`;

        comments.forEach((line) => output += `* ${line}\n`);

        return output += `*/\n`;
    }

    /**
     * Con solo ejecutar `gulp` se realiza el compilado de CSS y JS CONSTRUYE TODO DE NUEVO
     */
    gulp.task('default', ['sass', 'js']);

    /**
     * Para estar al pendiente de los cambios sin necesidad de ejecutar comandos de consola
     *
     * Para ejecutar esta tarea: `gulp watch`
     */
    gulp.task('watch', () => gulp.watch([paths.source.scss, paths.source.js], ['default']));
    

