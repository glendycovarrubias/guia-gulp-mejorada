<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class DefaultController extends Controller
{
    /**
     * @Route("/", options={"expose"=true}, name="homepage")
     */
    public function indexAction(Request $request)
    {   
        // replace this example code with whatever you need
        return $this->render('default/index.html.twig');
    }

    /**
     * @Route("/vista_actividad", options={"expose"=true}, name="vista_actividad")
     */
    public function vistaActividadAction(Request $request)
    {   
        // replace this example code with whatever you need
        return $this->render('Actividad/Actividad.html.twig');
    }
}
