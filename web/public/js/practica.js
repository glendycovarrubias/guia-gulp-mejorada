/*!
* Este archivo es parte del proyecto practica
* 
* practica ========
* 
* Practicando
* 
* @copyright Copyright (c);
* 
* @filesource
* 
* Hecho con <3 por Glendy
* 
* DEPENDENCIES:
* JQUERY
* Font Awesome
* 
* 
*/
//crea la funcion get Ajax para que podamos utilizar para no perer la info. de clientes y agencias
// function getAjax(ruta, fnSuccess)
// {   
//     console.log(ruta);
//     $.ajax({
//       url: ruta
//     }).done(function(data) {
//         console.log('Bien');
//     });
// } 
// 

// Namespace del módulo
// var ajax = {};

// (function($, _) {
//    this.saludarAjax = function(){
//      console.log('ajax');
//    }
// }).apply(ajax, [jQuery], _);


// var ajax = {
//     'saludar_mundo' : 'Hola ajax'
// }
"use strict";
'use strict';

$(document).ready(function () {
    console.log('Mejorando configuracion gulp');
    /*Rutas definidas*/
    // let rutes = 
    // {
    //     index    : '#'
    // };

    // //Se creo para poder usar la validacion del cliente ruta Ajax
    // getAjax(rutes.index, function(datos){
    //     //Vista donde cargaran las funciones
    //     $('#contedor_principal').empty().append(datos);                
    // });

    // ajax.saludarAjax();

    // var ajax = {
    //     'saludar_mundo' : 'Hola ajax'
    // };


    // console.log(ajax);
    // 

    var elementos = {
        contenedor: "#contedor_principal"
    };

    // console.log(elementos);
    var rutaVista = Routing.generate('homepage');

    $.ajax({
        url: rutaVista,
        context: document.body
    }).done(function () {
        var vista = Routing.generate('vista_actividad');
        $(elementos.contenedor).load(vista);
    });
});
"use strict";